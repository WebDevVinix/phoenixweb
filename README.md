# per-cms-an

Standard CMS Rails yang sudah dilengkapi dengan devise dan Template admin. Untuk CMS Website News

## Built With

* Rails 5.2.2 - Website Frameworks
* Ruby 2.5.1p57 - Programming Language
* Nice Admin - Admin Template (File Template ada di folder vendor)

## Author

Mohammad Husnul Rahmadi (husnul@phoenix.co.id) - Website Developer

digital@phoenix.co.id
