Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  root 'pages#home'
  get 'app' => "app#index"
  get 'pages' => "pages#index"

  get 'list-pages' => "pages#index", as: :list_pages
  get 'list-media' => "media#index", as: :list_media
  get 'list-book' => "books#index", as: :list_book
  get 'list-service' => "services#index", as: :list_service
  get 'list-corporate' => "corporates#index", as: :list_corporate
  get 'list-article' => "articles#index", as: :list_news
  get 'list-contact' => "contacts#index", as: :list_contact
  get 'phoenix-news' => "pages#ph_news", as: :ph_news
  get 'connect-with-us' => "pages#contact", as: :create_message

  get '/:id', to: 'pages#show', as: :pages_show

  resources :books,path:'publishing'
  resources :services,path:'creative-services'
  resources :corporates,path:'corporate-recognition'
  resources :articles,path:'phoenix-news'
  resources :authors
  resources :corporate_details
  resources :contacts
  resources :pages
  resources :users
  resources :media
  resources :book_previews
  resources :setups

  devise_for :users,
  path:'user',
  path_names: {
    sign_in: 'login',
    sign_out: 'logout'
  }

  get ':category_name/:sub/:id', to: 'corporate_details#show', as: :dtl_sub_category

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
