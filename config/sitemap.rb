# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.phoenix.co.id/"

SitemapGenerator::Sitemap.create do
  add root_path, :changefreq => 'monthly'
  add ph_news_path, :changefreq => 'monthly'
  add create_message_path, :changefreq => 'monthly'
  Page.all.each do |x|
    add  "/#{x.slug}", :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Medium.all.each do |x|
    add  "/media/#{x.slug}", :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Book.all.each do |x|
    add  "/publishing/#{x.slug}", :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Article.all.each do |x|
    add  "/phoenix-news/#{x.slug}", :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Service.all.each do |x|
    add  "/creative-services/#{x.slug}", :changefreq => 'monthly', :lastmod => x.updated_at
  end
  Corporate.all.each do |x|
    add  "/corporate-recognition/#{x.slug}", :changefreq => 'monthly', :lastmod => x.updated_at
  end
end
