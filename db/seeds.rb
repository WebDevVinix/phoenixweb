# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
u = User.new
u.username = "administration"
u.role = "root"
u.email = "admin@phoenix.co.id"
u.password = "password"
u.save

s = Setup.new
s.id = 1
s.site_name = 'Phoenix Communications'
s.site_title = 'Media, Publishing, Corporate Recognition and Creative Services'
s.site_url = 'https://phoenix.co.id'
s.save
