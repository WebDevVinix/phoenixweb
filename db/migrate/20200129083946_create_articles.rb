class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :slug
      t.string :meta_description
      t.text :content
      t.string :image
      t.references :author, foreign_key: true

      t.timestamps
    end
  end
end
