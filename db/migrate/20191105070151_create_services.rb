class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :title
      t.text :meta_description
      t.string :image
      t.text :content
      t.string :slug
      t.string :category

      t.timestamps
    end
  end
end
