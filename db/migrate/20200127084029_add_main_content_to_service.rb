class AddMainContentToService < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :main_content, :text
    add_column :services, :image_cover, :string
  end
end
