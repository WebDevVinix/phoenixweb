class AddPublishToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :publish, :date
  end
end
