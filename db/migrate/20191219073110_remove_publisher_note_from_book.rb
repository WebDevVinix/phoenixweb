class RemovePublisherNoteFromBook < ActiveRecord::Migration[5.2]
  def change
    remove_column :books, :publisher_note, :string
  end
end
