class AddWriterToPublish < ActiveRecord::Migration[5.2]
  def change
    add_column :publishes, :writer, :string
  end
end
