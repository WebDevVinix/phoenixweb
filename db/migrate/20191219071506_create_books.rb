class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.text :meta_description
      t.string :author
      t.text :description
      t.string :cover
      t.string :main_image
      t.string :price
      t.string :link
      t.string :tokopedia
      t.string :bukalapak
      t.string :publisher_note

      t.timestamps
    end
  end
end
