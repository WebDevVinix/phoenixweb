class CreateSetups < ActiveRecord::Migration[5.2]
  def change
    create_table :setups do |t|
      t.string :site_name
      t.string :site_title
      t.text   :site_description
      t.string :site_keywords
      t.string :site_url
      t.string :site_image
      t.string :site_favicon

      t.timestamps
    end
  end
end
