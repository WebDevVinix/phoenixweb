class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.text :content
      t.string :seo_title
      t.text :meta_description
      t.string :focus_keyword

      t.timestamps
    end
    add_index :pages, :slug, unique: true
  end
end
