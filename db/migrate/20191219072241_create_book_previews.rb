class CreateBookPreviews < ActiveRecord::Migration[5.2]
  def change
    create_table :book_previews do |t|
      t.string :image
      t.string :caption
      t.references :book, foreign_key: true
      t.string :category

      t.timestamps
    end
  end
end
