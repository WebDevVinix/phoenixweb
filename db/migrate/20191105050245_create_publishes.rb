class CreatePublishes < ActiveRecord::Migration[5.2]
  def change
    create_table :publishes do |t|
      t.string :title
      t.string :cover
      t.text :meta_description
      t.string :image_intro
      t.text :content
      t.string :slug

      t.timestamps
    end
  end
end
