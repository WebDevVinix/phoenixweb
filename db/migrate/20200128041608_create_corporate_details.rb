class CreateCorporateDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :corporate_details do |t|
      t.string :title
      t.text :meta_description
      t.string :slug
      t.text :content
      t.string :image
      t.string :image_cover
      t.references :corporate, foreign_key: true

      t.timestamps
    end
  end
end
