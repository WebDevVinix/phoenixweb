class AddPublisherNoteToBook < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :publisher_note, :text
  end
end
