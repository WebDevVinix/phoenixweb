class CreateCorporates < ActiveRecord::Migration[5.2]
  def change
    create_table :corporates do |t|
      t.string :title
      t.text :meta_description
      t.string :slug
      t.text :content
      t.string :image
      t.string :image_cover

      t.timestamps
    end
  end
end
