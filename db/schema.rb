# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_29_104723) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.string "meta_description"
    t.text "content"
    t.string "image"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "publish"
    t.index ["author_id"], name: "index_articles_on_author_id"
  end

  create_table "authors", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
  end

  create_table "book_previews", force: :cascade do |t|
    t.string "image"
    t.string "caption"
    t.bigint "book_id"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id"], name: "index_book_previews_on_book_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "title"
    t.text "meta_description"
    t.string "author"
    t.text "description"
    t.string "cover"
    t.string "main_image"
    t.string "price"
    t.string "link"
    t.string "tokopedia"
    t.string "bukalapak"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "publisher_note"
    t.string "sub_title"
    t.string "slug"
    t.text "book_info"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "fullname"
    t.string "phonenumber"
    t.string "email"
    t.string "subject"
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "corporate_details", force: :cascade do |t|
    t.string "title"
    t.text "meta_description"
    t.string "slug"
    t.text "content"
    t.string "image"
    t.string "image_cover"
    t.bigint "corporate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["corporate_id"], name: "index_corporate_details_on_corporate_id"
  end

  create_table "corporates", force: :cascade do |t|
    t.string "title"
    t.text "meta_description"
    t.string "slug"
    t.text "content"
    t.string "image"
    t.string "image_cover"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "media", force: :cascade do |t|
    t.string "title"
    t.string "meta_description"
    t.string "slug"
    t.text "content"
    t.string "image"
    t.string "image_cover"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "content"
    t.string "seo_title"
    t.text "meta_description"
    t.string "focus_keyword"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "image_thumbail"
    t.index ["slug"], name: "index_pages_on_slug", unique: true
  end

  create_table "publishes", force: :cascade do |t|
    t.string "title"
    t.string "cover"
    t.text "meta_description"
    t.string "image_intro"
    t.text "content"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "writer"
  end

  create_table "services", force: :cascade do |t|
    t.string "title"
    t.text "meta_description"
    t.string "image"
    t.text "content"
    t.string "slug"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "main_content"
    t.string "image_cover"
  end

  create_table "setups", force: :cascade do |t|
    t.string "site_name"
    t.string "site_title"
    t.text "site_description"
    t.string "site_keywords"
    t.string "site_url"
    t.string "site_image"
    t.string "site_favicon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "name"
    t.string "slug"
    t.string "role"
    t.string "gender"
    t.text "bio"
    t.string "avatar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
  end

  add_foreign_key "articles", "authors"
  add_foreign_key "book_previews", "books"
  add_foreign_key "corporate_details", "corporates"
end
