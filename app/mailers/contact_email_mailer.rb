class ContactEmailMailer < ApplicationMailer
  default from: "Phoenix Communications <no-reply@phoenix.co.id>"

  def new_contact(contact)
   @contact = contact
   mail( :to => 'aulya@phoenix.co.id',
     :subject => 'New Message From Phoenix Website',
     :display_name => "Phoenix Communications" )
  end

end
