class ApplicationController < ActionController::Base
  before_action :prepare_meta_tags

  private
  def prepare_meta_tags(options={})
    site_name   = 'Phoenix Communications'
    title       = 'Media, Publishing, Corporate Recognition and Creative Services'
    description = 'Phoenix Communications is the publisher of NOW! Jakarta and NOW! Bali magazine. We also offer book publication as well as creative services. We support sustainability with Most Valued Business (MVB) and Say Yes to Less programme.'
    keywords    = 'Media, Publishing, Corporate Recognition and Creative Services'
    image       = request.protocol + request.host_with_port + view_context.image_path('slider/P1.2 - Publishing - Slider.JPG')
    current_url = "https://phoenix.co.id"

    # Let's prepare a nice set of defaults
    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    keywords,
      reverse: true,
      twitter: {
        site_name: site_name,
        site: '@phoenix_IDN',
        creator: '@phoenix_IDN',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end
end
