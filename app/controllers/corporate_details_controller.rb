class CorporateDetailsController < ApplicationController
  before_action :set_corporate_detail, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :edit, :new]
  layout 'admin', only: [:index, :edit, :new]
  # GET /corporate_details
  # GET /corporate_details.json
  def index
    @corporate_details = CorporateDetail.all
  end

  # GET /corporate_details/1
  # GET /corporate_details/1.json
  def show
    prepare_meta_tags(
    title: @corporate_detail.title,
    description: @corporate_detail.meta_description,
    image: @corporate_detail.image,
    twitter: {
      site_name: "Phoenix Communications | #{@corporate_detail.title}" ,
      site: '@phoenix_IDN',
      creator: '@phoenix_IDN',
      card: 'summary',
      description:   @corporate_detail.meta_description || ActionView::Base.full_sanitizer.sanitize(@corporate_detail.content).truncate(250),
      title: @corporate_detail.title,
      image: request.protocol + request.host_with_port + @corporate_detail.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @corporate_detail.slug,
      site_name:  "Phoenix Communications | #{@corporate_detail.title}" ,
      title: @corporate_detail.title,
      image: request.protocol + request.host_with_port + @corporate_detail.image.url,
      type: 'article',
      description:  @corporate_detail.meta_description || ActionView::Base.full_sanitizer.sanitize(@corporate_detail.content).truncate(250)
      })
  end

  # GET /corporate_details/new
  def new
    @corporate_detail = CorporateDetail.new
  end

  # GET /corporate_details/1/edit
  def edit
  end

  # POST /corporate_details
  # POST /corporate_details.json
  def create
    @corporate_detail = CorporateDetail.new(corporate_detail_params)

    respond_to do |format|
      if @corporate_detail.save
        format.html { redirect_to @corporate_detail, notice: 'Corporate detail was successfully created.' }
        format.json { render :show, status: :created, location: @corporate_detail }
      else
        format.html { render :new }
        format.json { render json: @corporate_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /corporate_details/1
  # PATCH/PUT /corporate_details/1.json
  def update
    respond_to do |format|
      if @corporate_detail.update(corporate_detail_params)
        format.html { redirect_to @corporate_detail, notice: 'Corporate detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @corporate_detail }
      else
        format.html { render :edit }
        format.json { render json: @corporate_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /corporate_details/1
  # DELETE /corporate_details/1.json
  def destroy
    @corporate_detail.destroy
    respond_to do |format|
      format.html { redirect_to corporate_details_url, notice: 'Corporate detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_corporate_detail
      @corporate_detail = CorporateDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def corporate_detail_params
      params.require(:corporate_detail).permit(:title, :meta_description, :slug, :content, :image, :image_cover, :corporate_id)
    end
end
