class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!,only: [:index, :edit, :new]
  layout 'admin', only: [:index, :edit, :new]
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    prepare_meta_tags(
    title: @page.title,
    description: @page.meta_description,
    image: @page.image,
    twitter: {
      site_name: "Phoenix Communications | #{@page.title}" ,
      site: '@phoenix_IDN',
      creator: '@phoenix_IDN',
      card: 'summary',
      description:   @page.meta_description || ActionView::Base.full_sanitizer.sanitize(@page.content).truncate(250),
      title: @page.title,
      image: request.protocol + request.host_with_port + @page.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @page.slug,
      site_name:  "Phoenix Communications | #{@page.title}" ,
      title: @page.title,
      image: request.protocol + request.host_with_port + @page.image.url,
      type: 'article',
      description:  @page.meta_description || ActionView::Base.full_sanitizer.sanitize(@page.content).truncate(250)
      })
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to pages_path, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to pages_path, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def home
    @articles = Article.all.limit(2)
  end

  def contact
    @contact = Contact.new
  end

  def ph_news
      @articles = Article.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :slug, :content, :seo_title, :meta_description, :focus_keyword, :image, :image_thumbail, :slug)
    end
end
