class CorporatesController < ApplicationController
  before_action :set_corporate, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :edit, :new]
  layout 'admin', only: [:index, :edit, :new]
  # GET /corporates
  # GET /corporates.json
  def index
    @corporates = Corporate.all
  end

  # GET /corporates/1
  # GET /corporates/1.json
  def show
    prepare_meta_tags(
    title: @corporate.title,
    description: @corporate.meta_description,
    image: @corporate.image,
    twitter: {
      site_name: "Phoenix Communications | #{@corporate.title}" ,
      site: '@phoenix_IDN',
      creator: '@phoenix_IDN',
      card: 'summary',
      description:   @corporate.meta_description || ActionView::Base.full_sanitizer.sanitize(@corporate.content).truncate(250),
      title: @corporate.title,
      image: request.protocol + request.host_with_port + @corporate.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @corporate.slug,
      site_name:  "Phoenix Communications | #{@corporate.title}" ,
      title: @corporate.title,
      image: request.protocol + request.host_with_port + @corporate.image.url,
      type: 'article',
      description:  @corporate.meta_description || ActionView::Base.full_sanitizer.sanitize(@corporate.content).truncate(250)
      })
  end

  # GET /corporates/new
  def new
    @corporate = Corporate.new
  end

  # GET /corporates/1/edit
  def edit
  end

  # POST /corporates
  # POST /corporates.json
  def create
    @corporate = Corporate.new(corporate_params)

    respond_to do |format|
      if @corporate.save
        format.html { redirect_to @corporate, notice: 'Corporate was successfully created.' }
        format.json { render :show, status: :created, location: @corporate }
      else
        format.html { render :new }
        format.json { render json: @corporate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /corporates/1
  # PATCH/PUT /corporates/1.json
  def update
    respond_to do |format|
      if @corporate.update(corporate_params)
        format.html { redirect_to @corporate, notice: 'Corporate was successfully updated.' }
        format.json { render :show, status: :ok, location: @corporate }
      else
        format.html { render :edit }
        format.json { render json: @corporate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /corporates/1
  # DELETE /corporates/1.json
  def destroy
    @corporate.destroy
    respond_to do |format|
      format.html { redirect_to corporates_url, notice: 'Corporate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_corporate
      @corporate = Corporate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def corporate_params
      params.require(:corporate).permit(:title, :meta_description, :slug, :content, :image, :image_cover)
    end
end
