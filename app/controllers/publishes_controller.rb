class PublishesController < ApplicationController
  before_action :set_publish, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, execpt: :show
  layout 'admin', only: [:index, :edit, :new]
  # GET /publishes
  # GET /publishes.json
  def index
    @publishes = Publish.all
  end

  # GET /publishes/1
  # GET /publishes/1.json
  def show
    prepare_meta_tags(
    title: @publish.title,
    description: @publish.meta_description,
    image: @publish.cover,
    twitter: {
      site_name: "Phoenix Communications | #{@publish.title}" ,
      site: '@phoenix_IDN',
      creator: '@phoenix_IDN',
      card: 'summary',
      description:   @publish.meta_description || ActionView::Base.full_sanitizer.sanitize(@publish.content).truncate(250),
      title: @publish.title,
      image: request.protocol + request.host_with_port + @publish.cover.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @publish.slug,
      site_name:  "Phoenix Communications | #{@publish.title}" ,
      title: @publish.title,
      image: request.protocol + request.host_with_port + @publish.cover.url,
      type: 'article',
      description:  @publish.meta_description || ActionView::Base.full_sanitizer.sanitize(@publish.content).truncate(250)
      })
  end

  # GET /publishes/new
  def new
    @publish = Publish.new
  end

  # GET /publishes/1/edit
  def edit
  end

  # POST /publishes
  # POST /publishes.json
  def create
    @publish = Publish.new(publish_params)

    respond_to do |format|
      if @publish.save
        format.html { redirect_to @publish, notice: 'Publish was successfully created.' }
        format.json { render :show, status: :created, location: @publish }
      else
        format.html { render :new }
        format.json { render json: @publish.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /publishes/1
  # PATCH/PUT /publishes/1.json
  def update
    respond_to do |format|
      if @publish.update(publish_params)
        format.html { redirect_to @publish, notice: 'Publish was successfully updated.' }
        format.json { render :show, status: :ok, location: @publish }
      else
        format.html { render :edit }
        format.json { render json: @publish.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publishes/1
  # DELETE /publishes/1.json
  def destroy
    @publish.destroy
    respond_to do |format|
      format.html { redirect_to publishes_url, notice: 'Publish was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_publish
      @publish = Publish.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def publish_params
      params.require(:publish).permit(:title, :cover, :meta_description, :image_intro, :content, :slug, :writer)
    end
end
