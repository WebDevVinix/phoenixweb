class BookPreviewsController < ApplicationController
  before_action :set_book_preview, only: [:show, :edit, :update, :destroy]

  # GET /book_previews
  # GET /book_previews.json
  def index
    @book_previews = BookPreview.all
  end

  # GET /book_previews/1
  # GET /book_previews/1.json
  def show
  end

  # GET /book_previews/new
  def new
    @book_preview = BookPreview.new
  end

  # GET /book_previews/1/edit
  def edit
  end

  # POST /book_previews
  # POST /book_previews.json
  def create
    @book_preview = BookPreview.new(book_preview_params)

    respond_to do |format|
      if @book_preview.save
        format.html { redirect_to @book_preview, notice: 'Book preview was successfully created.' }
        format.json { render :show, status: :created, location: @book_preview }
      else
        format.html { render :new }
        format.json { render json: @book_preview.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /book_previews/1
  # PATCH/PUT /book_previews/1.json
  def update
    respond_to do |format|
      if @book_preview.update(book_preview_params)
        format.html { redirect_to @book_preview, notice: 'Book preview was successfully updated.' }
        format.json { render :show, status: :ok, location: @book_preview }
      else
        format.html { render :edit }
        format.json { render json: @book_preview.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /book_previews/1
  # DELETE /book_previews/1.json
  def destroy
    @book_preview.destroy
    respond_to do |format|
      format.html { redirect_to book_previews_url, notice: 'Book preview was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book_preview
      @book_preview = BookPreview.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_preview_params
      params.require(:book_preview).permit(:image, :caption, :book_id, :category)
    end
end
