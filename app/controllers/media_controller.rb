class MediaController < ApplicationController
  before_action :set_medium, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!,only: [:index, :edit, :new]
  layout 'admin', only: [:index, :edit, :new]

  # GET /media
  # GET /media.json
  def index
    @media = Medium.all
  end

  # GET /media/1
  # GET /media/1.json
  def show
    prepare_meta_tags(
    title: @medium.title,
    description: @medium.meta_description,
    image: @medium.image,
    twitter: {
      site_name: "Phoenix Communications | #{@medium.title}" ,
      site: '@phoenix_IDN',
      creator: '@phoenix_IDN',
      card: 'summary',
      description:   @medium.meta_description || ActionView::Base.full_sanitizer.sanitize(@medium.content).truncate(250),
      title: @medium.title,
      image: request.protocol + request.host_with_port + @medium.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @medium.slug,
      site_name:  "Phoenix Communications | #{@medium.title}" ,
      title: @medium.title,
      image: request.protocol + request.host_with_port + @medium.image.url,
      type: 'article',
      description:  @medium.meta_description || ActionView::Base.full_sanitizer.sanitize(@medium.content).truncate(250)
      })
  end

  # GET /media/new
  def new
    @medium = Medium.new
  end

  # GET /media/1/edit
  def edit
  end

  # POST /media
  # POST /media.json
  def create
    @medium = Medium.new(medium_params)

    respond_to do |format|
      if @medium.save
        format.html { redirect_to @medium, notice: 'Medium was successfully created.' }
        format.json { render :show, status: :created, location: @medium }
      else
        format.html { render :new }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /media/1
  # PATCH/PUT /media/1.json
  def update
    respond_to do |format|
      if @medium.update(medium_params)
        format.html { redirect_to @medium, notice: 'Medium was successfully updated.' }
        format.json { render :show, status: :ok, location: @medium }
      else
        format.html { render :edit }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /media/1
  # DELETE /media/1.json
  def destroy
    @medium.destroy
    respond_to do |format|
      format.html { redirect_to media_url, notice: 'Medium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medium
      @medium = Medium.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medium_params
      params.require(:medium).permit(:title, :meta_description, :slug, :content, :image, :image_cover, :publish)
    end
end
