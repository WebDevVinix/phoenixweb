class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :edit, :new]
  layout 'admin', only: [:index, :edit, :new]
  # GET /services
  # GET /services.json
  def index
    @services = Service.all
  end

  # GET /services/1
  # GET /services/1.json
  def show
    prepare_meta_tags(
    title: @service.title,
    description: @service.meta_description,
    image: @service.image,
    twitter: {
      site_name: "Phoenix Communications | #{@service.title}" ,
      site: '@phoenix_IDN',
      creator: '@phoenix_IDN',
      card: 'summary',
      description:   @service.meta_description || ActionView::Base.full_sanitizer.sanitize(@service.content).truncate(250),
      title: @service.title,
      image: request.protocol + request.host_with_port + @service.image.url
    },
    og: {
      url: request.protocol + request.host_with_port+"/"+ @service.slug,
      site_name:  "Phoenix Communications | #{@service.title}" ,
      title: @service.title,
      image: request.protocol + request.host_with_port + @service.image.url,
      type: 'article',
      description:  @service.meta_description || ActionView::Base.full_sanitizer.sanitize(@service.content).truncate(250)
      })
  end

  # GET /services/new
  def new
    @service = Service.new
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services
  # POST /services.json
  def create
    @service = Service.new(service_params)

    respond_to do |format|
      if @service.save
        format.html { redirect_to @service, notice: 'Service was successfully created.' }
        format.json { render :show, status: :created, location: @service }
      else
        format.html { render :new }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    respond_to do |format|
      if @service.update(service_params)
        format.html { redirect_to @service, notice: 'Service was successfully updated.' }
        format.json { render :show, status: :ok, location: @service }
      else
        format.html { render :edit }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service.destroy
    respond_to do |format|
      format.html { redirect_to services_url, notice: 'Service was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:title, :meta_description, :image,:image_cover, :main_content, :content, :slug, :category)
    end
end
