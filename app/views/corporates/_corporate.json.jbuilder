json.extract! corporate, :id, :title, :meta_description, :slug, :content, :image, :image_cover, :created_at, :updated_at
json.url corporate_url(corporate, format: :json)
