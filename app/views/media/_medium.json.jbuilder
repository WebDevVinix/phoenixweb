json.extract! medium, :id, :title, :meta_description, :slug, :content, :image, :image_cover, :created_at, :updated_at
json.url medium_url(medium, format: :json)
