json.extract! book, :id, :title, :meta_description, :author, :description, :cover, :main_image, :price, :link, :tokopedia, :bukalapak, :publisher_note, :created_at, :updated_at
json.url book_url(book, format: :json)
