json.extract! setup, :id, :site_name, :site_title, :site_description, :site_keywords, :site_url, :site_image, :created_at, :updated_at
json.url setup_url(setup, format: :json)
