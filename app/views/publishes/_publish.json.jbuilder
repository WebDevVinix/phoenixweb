json.extract! publish, :id, :title, :cover, :meta_description, :image_intro, :content, :slug, :created_at, :updated_at
json.url publish_url(publish, format: :json)
