json.extract! service, :id, :title, :meta_description, :image, :content, :slug, :category, :created_at, :updated_at
json.url service_url(service, format: :json)
