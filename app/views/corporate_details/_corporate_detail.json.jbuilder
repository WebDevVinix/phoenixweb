json.extract! corporate_detail, :id, :title, :meta_description, :slug, :content, :image, :image_cover, :corporate_id, :created_at, :updated_at
json.url corporate_detail_url(corporate_detail, format: :json)
