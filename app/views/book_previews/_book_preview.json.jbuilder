json.extract! book_preview, :id, :image, :caption, :book_id, :category, :created_at, :updated_at
json.url book_preview_url(book_preview, format: :json)
