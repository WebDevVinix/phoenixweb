class Service < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image, ImageUploader
  mount_uploader :image_cover, ImageUploader

  Category = [ "marketing", "creative" ]

end
