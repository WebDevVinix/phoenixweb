class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image, ImageUploader
  belongs_to :author

  default_scope { order(publish: :desc) }
end
