class Author < ApplicationRecord
  extend FriendlyId
  friendly_id :name

  has_many :articles
end
