class Medium < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image_cover, ImageUploader
  mount_uploader :image, ImageUploader

  class << self

    def nj
      Medium.find("now-jakarta")
    end

    def nb
      Medium.find("now-bali")
    end

    def brbca_nj
      Medium.find("now-jakarta-best-restaurant-bar-cafe-awards")
    end

    def brbca_nb
      Medium.find("now-bali-best-restaurant-bar-and-cafe-awards")
    end

    def njsc
      Medium.find("now-jakarta-schools-competition")
    end

    def nowstudy
      Medium.find("now-study")
    end

  end


end
