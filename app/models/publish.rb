class Publish < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :cover, ImageUploader
  mount_uploader :image_intro, ImageUploader
end
