class Book < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :cover, ImageUploader
  mount_uploader :main_image, ImageUploader

  has_many :book_previews, dependent: :delete_all
  accepts_nested_attributes_for :book_previews, reject_if: :all_blank, allow_destroy: true


end
