class Page < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image_thumbail, ImageUploader
  mount_uploader :image, ImageUploader

  validates :title, presence: true

  class << self

    def media
      Page.find("media")
    end

    def publishing
      Page.find("publishing")
    end

    def creative
      Page.find("creative-services")
    end

    def corporate
      Page.find("corporate-recognition")
    end

    def about
      Page.find("about-us")
    end

  end

end
