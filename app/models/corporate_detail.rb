class CorporateDetail < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image_cover, ImageUploader
  mount_uploader :image, ImageUploader
  belongs_to :corporate
end
