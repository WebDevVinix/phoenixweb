class Setup < ApplicationRecord
  mount_uploader :site_image, LogoUploader
  mount_uploader :site_favicon, IconUploader
  validates :site_name, presence: true
  validates :site_title, presence: true
  validates :site_url, presence: true
end
