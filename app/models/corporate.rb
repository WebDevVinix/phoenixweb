class Corporate < ApplicationRecord
  extend FriendlyId
  friendly_id :title
  mount_uploader :image_cover, ImageUploader
  mount_uploader :image, ImageUploader

  class << self
    def mvb
      Corporate.find("most-valued-business-mvb-indonesia")
    end

  end
end
