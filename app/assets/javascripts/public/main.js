$('.home-slider').owlCarousel({
  autoplay:true,
  autoplayTimeout:15000,
  autoplayHoverPause:false,
  items:1,
  nav:false,
  loop:true,
  animateOut: 'fadeOut',
  dots: false
})

$('.client-slide').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:15000,
    margin:10,
    nav:false,
    dots: false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:5
        },
        1000:{
            items:5
        }
    }
})


$('.item-desc').matchHeight();
$('.item-descx').matchHeight();
$('.item-descy').matchHeight();
$('.sm-crd-hgt').matchHeight();
$('.sm-crd-nws').matchHeight();

$(".video-0,.video-1, .video-2, .video-3, .video-4, .video-5, .video-6, .video-7, .video-8").on("click", function(event) {
  event.preventDefault();
  $(".video_case iframe").prop("src", $(event.currentTarget).attr("href"));
});

$(".img-0,.img-1, .img-2, .img-3, .img-4, .img-5, .img-6, .img-7, .img-8").on("click", function(event) {
  event.preventDefault();
  $(".img_case img").prop("src", $(event.currentTarget).attr("href"));
});

// sss
