require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @book = books(:one)
  end

  test "should get index" do
    get books_url
    assert_response :success
  end

  test "should get new" do
    get new_book_url
    assert_response :success
  end

  test "should create book" do
    assert_difference('Book.count') do
      post books_url, params: { book: { author: @book.author, bukalapak: @book.bukalapak, cover: @book.cover, description: @book.description, link: @book.link, main_image: @book.main_image, meta_description: @book.meta_description, price: @book.price, publisher_note: @book.publisher_note, title: @book.title, tokopedia: @book.tokopedia } }
    end

    assert_redirected_to book_url(Book.last)
  end

  test "should show book" do
    get book_url(@book)
    assert_response :success
  end

  test "should get edit" do
    get edit_book_url(@book)
    assert_response :success
  end

  test "should update book" do
    patch book_url(@book), params: { book: { author: @book.author, bukalapak: @book.bukalapak, cover: @book.cover, description: @book.description, link: @book.link, main_image: @book.main_image, meta_description: @book.meta_description, price: @book.price, publisher_note: @book.publisher_note, title: @book.title, tokopedia: @book.tokopedia } }
    assert_redirected_to book_url(@book)
  end

  test "should destroy book" do
    assert_difference('Book.count', -1) do
      delete book_url(@book)
    end

    assert_redirected_to books_url
  end
end
