require 'test_helper'

class CorporateDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @corporate_detail = corporate_details(:one)
  end

  test "should get index" do
    get corporate_details_url
    assert_response :success
  end

  test "should get new" do
    get new_corporate_detail_url
    assert_response :success
  end

  test "should create corporate_detail" do
    assert_difference('CorporateDetail.count') do
      post corporate_details_url, params: { corporate_detail: { content: @corporate_detail.content, corporate_id: @corporate_detail.corporate_id, image: @corporate_detail.image, image_cover: @corporate_detail.image_cover, meta_description: @corporate_detail.meta_description, slug: @corporate_detail.slug, title: @corporate_detail.title } }
    end

    assert_redirected_to corporate_detail_url(CorporateDetail.last)
  end

  test "should show corporate_detail" do
    get corporate_detail_url(@corporate_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_corporate_detail_url(@corporate_detail)
    assert_response :success
  end

  test "should update corporate_detail" do
    patch corporate_detail_url(@corporate_detail), params: { corporate_detail: { content: @corporate_detail.content, corporate_id: @corporate_detail.corporate_id, image: @corporate_detail.image, image_cover: @corporate_detail.image_cover, meta_description: @corporate_detail.meta_description, slug: @corporate_detail.slug, title: @corporate_detail.title } }
    assert_redirected_to corporate_detail_url(@corporate_detail)
  end

  test "should destroy corporate_detail" do
    assert_difference('CorporateDetail.count', -1) do
      delete corporate_detail_url(@corporate_detail)
    end

    assert_redirected_to corporate_details_url
  end
end
