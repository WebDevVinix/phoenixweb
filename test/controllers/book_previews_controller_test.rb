require 'test_helper'

class BookPreviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @book_preview = book_previews(:one)
  end

  test "should get index" do
    get book_previews_url
    assert_response :success
  end

  test "should get new" do
    get new_book_preview_url
    assert_response :success
  end

  test "should create book_preview" do
    assert_difference('BookPreview.count') do
      post book_previews_url, params: { book_preview: { book_id: @book_preview.book_id, caption: @book_preview.caption, category: @book_preview.category, image: @book_preview.image } }
    end

    assert_redirected_to book_preview_url(BookPreview.last)
  end

  test "should show book_preview" do
    get book_preview_url(@book_preview)
    assert_response :success
  end

  test "should get edit" do
    get edit_book_preview_url(@book_preview)
    assert_response :success
  end

  test "should update book_preview" do
    patch book_preview_url(@book_preview), params: { book_preview: { book_id: @book_preview.book_id, caption: @book_preview.caption, category: @book_preview.category, image: @book_preview.image } }
    assert_redirected_to book_preview_url(@book_preview)
  end

  test "should destroy book_preview" do
    assert_difference('BookPreview.count', -1) do
      delete book_preview_url(@book_preview)
    end

    assert_redirected_to book_previews_url
  end
end
