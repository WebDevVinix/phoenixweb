require 'test_helper'

class SetupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @setup = setups(:one)
  end

  test "should get index" do
    get setups_url
    assert_response :success
  end

  test "should get new" do
    get new_setup_url
    assert_response :success
  end

  test "should create setup" do
    assert_difference('Setup.count') do
      post setups_url, params: { setup: { site_description: @setup.site_description, site_image: @setup.site_image, site_keywords: @setup.site_keywords, site_name: @setup.site_name, site_title: @setup.site_title, site_url: @setup.site_url } }
    end

    assert_redirected_to setup_url(Setup.last)
  end

  test "should show setup" do
    get setup_url(@setup)
    assert_response :success
  end

  test "should get edit" do
    get edit_setup_url(@setup)
    assert_response :success
  end

  test "should update setup" do
    patch setup_url(@setup), params: { setup: { site_description: @setup.site_description, site_image: @setup.site_image, site_keywords: @setup.site_keywords, site_name: @setup.site_name, site_title: @setup.site_title, site_url: @setup.site_url } }
    assert_redirected_to setup_url(@setup)
  end

  test "should destroy setup" do
    assert_difference('Setup.count', -1) do
      delete setup_url(@setup)
    end

    assert_redirected_to setups_url
  end
end
