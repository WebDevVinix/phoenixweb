require "application_system_test_case"

class CorporateDetailsTest < ApplicationSystemTestCase
  setup do
    @corporate_detail = corporate_details(:one)
  end

  test "visiting the index" do
    visit corporate_details_url
    assert_selector "h1", text: "Corporate Details"
  end

  test "creating a Corporate detail" do
    visit corporate_details_url
    click_on "New Corporate Detail"

    fill_in "Content", with: @corporate_detail.content
    fill_in "Corporate", with: @corporate_detail.corporate_id
    fill_in "Image", with: @corporate_detail.image
    fill_in "Image cover", with: @corporate_detail.image_cover
    fill_in "Meta description", with: @corporate_detail.meta_description
    fill_in "Slug", with: @corporate_detail.slug
    fill_in "Title", with: @corporate_detail.title
    click_on "Create Corporate detail"

    assert_text "Corporate detail was successfully created"
    click_on "Back"
  end

  test "updating a Corporate detail" do
    visit corporate_details_url
    click_on "Edit", match: :first

    fill_in "Content", with: @corporate_detail.content
    fill_in "Corporate", with: @corporate_detail.corporate_id
    fill_in "Image", with: @corporate_detail.image
    fill_in "Image cover", with: @corporate_detail.image_cover
    fill_in "Meta description", with: @corporate_detail.meta_description
    fill_in "Slug", with: @corporate_detail.slug
    fill_in "Title", with: @corporate_detail.title
    click_on "Update Corporate detail"

    assert_text "Corporate detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Corporate detail" do
    visit corporate_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Corporate detail was successfully destroyed"
  end
end
