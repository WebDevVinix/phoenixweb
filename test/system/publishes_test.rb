require "application_system_test_case"

class PublishesTest < ApplicationSystemTestCase
  setup do
    @publish = publishes(:one)
  end

  test "visiting the index" do
    visit publishes_url
    assert_selector "h1", text: "Publishes"
  end

  test "creating a Publish" do
    visit publishes_url
    click_on "New Publish"

    fill_in "Content", with: @publish.content
    fill_in "Cover", with: @publish.cover
    fill_in "Image intro", with: @publish.image_intro
    fill_in "Meta description", with: @publish.meta_description
    fill_in "Slug", with: @publish.slug
    fill_in "Title", with: @publish.title
    click_on "Create Publish"

    assert_text "Publish was successfully created"
    click_on "Back"
  end

  test "updating a Publish" do
    visit publishes_url
    click_on "Edit", match: :first

    fill_in "Content", with: @publish.content
    fill_in "Cover", with: @publish.cover
    fill_in "Image intro", with: @publish.image_intro
    fill_in "Meta description", with: @publish.meta_description
    fill_in "Slug", with: @publish.slug
    fill_in "Title", with: @publish.title
    click_on "Update Publish"

    assert_text "Publish was successfully updated"
    click_on "Back"
  end

  test "destroying a Publish" do
    visit publishes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Publish was successfully destroyed"
  end
end
