require "application_system_test_case"

class BookPreviewsTest < ApplicationSystemTestCase
  setup do
    @book_preview = book_previews(:one)
  end

  test "visiting the index" do
    visit book_previews_url
    assert_selector "h1", text: "Book Previews"
  end

  test "creating a Book preview" do
    visit book_previews_url
    click_on "New Book Preview"

    fill_in "Book", with: @book_preview.book_id
    fill_in "Caption", with: @book_preview.caption
    fill_in "Category", with: @book_preview.category
    fill_in "Image", with: @book_preview.image
    click_on "Create Book preview"

    assert_text "Book preview was successfully created"
    click_on "Back"
  end

  test "updating a Book preview" do
    visit book_previews_url
    click_on "Edit", match: :first

    fill_in "Book", with: @book_preview.book_id
    fill_in "Caption", with: @book_preview.caption
    fill_in "Category", with: @book_preview.category
    fill_in "Image", with: @book_preview.image
    click_on "Update Book preview"

    assert_text "Book preview was successfully updated"
    click_on "Back"
  end

  test "destroying a Book preview" do
    visit book_previews_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Book preview was successfully destroyed"
  end
end
