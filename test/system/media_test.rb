require "application_system_test_case"

class MediaTest < ApplicationSystemTestCase
  setup do
    @medium = media(:one)
  end

  test "visiting the index" do
    visit media_url
    assert_selector "h1", text: "Media"
  end

  test "creating a Medium" do
    visit media_url
    click_on "New Medium"

    fill_in "Content", with: @medium.content
    fill_in "Image", with: @medium.image
    fill_in "Image cover", with: @medium.image_cover
    fill_in "Meta description", with: @medium.meta_description
    fill_in "Slug", with: @medium.slug
    fill_in "Title", with: @medium.title
    click_on "Create Medium"

    assert_text "Medium was successfully created"
    click_on "Back"
  end

  test "updating a Medium" do
    visit media_url
    click_on "Edit", match: :first

    fill_in "Content", with: @medium.content
    fill_in "Image", with: @medium.image
    fill_in "Image cover", with: @medium.image_cover
    fill_in "Meta description", with: @medium.meta_description
    fill_in "Slug", with: @medium.slug
    fill_in "Title", with: @medium.title
    click_on "Update Medium"

    assert_text "Medium was successfully updated"
    click_on "Back"
  end

  test "destroying a Medium" do
    visit media_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medium was successfully destroyed"
  end
end
