require "application_system_test_case"

class CorporatesTest < ApplicationSystemTestCase
  setup do
    @corporate = corporates(:one)
  end

  test "visiting the index" do
    visit corporates_url
    assert_selector "h1", text: "Corporates"
  end

  test "creating a Corporate" do
    visit corporates_url
    click_on "New Corporate"

    fill_in "Content", with: @corporate.content
    fill_in "Image", with: @corporate.image
    fill_in "Image cover", with: @corporate.image_cover
    fill_in "Meta description", with: @corporate.meta_description
    fill_in "Slug", with: @corporate.slug
    fill_in "Title", with: @corporate.title
    click_on "Create Corporate"

    assert_text "Corporate was successfully created"
    click_on "Back"
  end

  test "updating a Corporate" do
    visit corporates_url
    click_on "Edit", match: :first

    fill_in "Content", with: @corporate.content
    fill_in "Image", with: @corporate.image
    fill_in "Image cover", with: @corporate.image_cover
    fill_in "Meta description", with: @corporate.meta_description
    fill_in "Slug", with: @corporate.slug
    fill_in "Title", with: @corporate.title
    click_on "Update Corporate"

    assert_text "Corporate was successfully updated"
    click_on "Back"
  end

  test "destroying a Corporate" do
    visit corporates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Corporate was successfully destroyed"
  end
end
